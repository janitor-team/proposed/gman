/********************* list.c **********************/
/********************* 1999.6.13 *******************/

#include "list.h"
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <glib.h>

#define LIST_INIT_SIZE 15
List::List()
{
	items = NULL;
	buffer_length = 0;
	count = 0;
};

List::~List()
{
	if(items) free(items);
};

void List::add_item(void * item)
{
	if(!items) {
		items = (void **)malloc((size_t)sizeof(void*)*LIST_INIT_SIZE);
		if(!items) {
			perror("class List: can not malloc mem when init:");
			exit(1);
		}
		buffer_length = LIST_INIT_SIZE;
	}
	else if(buffer_length == count){
		items = (void **)realloc((void*)items,sizeof(void *)*(buffer_length+=buffer_length));
		if(!items){
			perror("class List: mem not enought when realloc:");
			exit(1);
		}
	}
	items[count++] = item;
}

void * List::get_item(int handle)
{
	if(handle >= count || handle < 0) return NULL;
	return items[handle];
}

int List::reset_item(int handle, void * data)
{
	if(handle >= count || handle < 0) return 1;
	items[handle] = data;
	return 0;
}

int List::search_item(void * item)
{
	int i;
	for (i = 0;i < count && items[i] != item;i++);
	return (i == count)? -1:i;
}

int List::delete_item(int handle)
{
	g_return_val_if_fail(handle < count && handle >= 0, 1);
	if(handle != count-1) memmove(items+handle,
								  items+(handle+1),
								  (count-handle-1)*sizeof(void*));
	count--;
	return 0;
}

void List::insert_item(int handle, void * item)
{
	int i;
	if(!items) {
		items = (void **)malloc(sizeof(void*)*LIST_INIT_SIZE);
		if(!items) {
			perror("class List: can not malloc mem when init:");
			exit(1);
		}
		buffer_length = LIST_INIT_SIZE;
	}
	else if(buffer_length == count){
		items = (void **)realloc((void*)items,sizeof(void *)*(buffer_length+=buffer_length));
		if(!items){
			perror("class List: mem not enought when realloc:");
			exit(1);
		}
	}
	if(handle >= count)
		items[count++] = item;
	else {
		handle = handle <0? 0:handle;
		for(i = count++;i>handle;i--)
			items[i] = items[i-1];
		items[handle] = item;
	}
}

void List::delete_all()
{
	count = 0;
}

int List::get_items(void * buffer)
{
	memcpy((void *)buffer,(void *)items,count*sizeof(void*));
	return count;
}

int List::get_size()
{
	return count;
}

int List::meet_end(int handle)
{
	return (handle >= count)? 1:0;
}

/********************* class Dictionary ***************/

Dictionary::Dictionary()
{
	names = new List;
	values = new List;
}

Dictionary::~Dictionary()
{
	delete(names);
	delete(values);
}

void Dictionary::add_item(char * name,void * value)
{
	int i;
	if((i = search_item(name)) != -1) {
		values->reset_item(i,value);
	} 
	else {
		names->add_item(name);
		values->add_item(value);
	}
}

int Dictionary::search_item(char * name)
{
	int i,j;
	j = names->get_size();
	for(i = 0;i<j && strcmp(name,(char*)names->get_item(i));i++);
	return (i==j)? -1:i;
}

int Dictionary::have_item(char * name)
{
	return search_item(name) != -1;
}

void * Dictionary::get_value(char * name)
{
	int i;
	i = names->search_item(name);
	return (i==-1)? NULL:values->get_item(i);
}

int Dictionary::get_size()
{
	return names->get_size();
}

void * Dictionary::get_value(int i)
{
	return values->get_item(i);
}

char * Dictionary::get_name(int i)
{
	return (char*)names->get_item(i);
}

void Dictionary::set_value(int i, void * value)
{
	values->reset_item(i,value);
}

void Dictionary::delete_item(int i)
{
	names->delete_item(i);
	values->delete_item(i);
}

void Dictionary::display_items()
{
	int i,j;
	j = names->get_size();
	for(i = 0;i<j;i++)
		printf("%s\n",names->get_item(i));
}



