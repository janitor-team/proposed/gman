/********************* gman.c ********************************/
/*  gman
 *  Copyright (C) 1999 Xinkai Wang
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <stdio.h>
#include <stdlib.h>

#include <gtk/gtk.h>
#include "menu.h"
#include "context.h"
#include "gman.h"
#include "task.h"
#include "taskfunc.h"
#include "mandata.h"
#include <unistd.h>

void init_context();
pthread_mutex_t gtk_lock;
pthread_mutex_t context_lock;
pthread_mutex_t loading_man_path_lock;
AppContext * context;
int debuging;

/********************** main **********************/
TaskGroup * task_group;

int main(int argc, char *argv[]) 
{
	GtkWidget *window;
	pthread_t th_init_data;

	pthread_mutex_init(&gtk_lock,NULL);
	pthread_mutex_init(&context_lock,NULL);
	pthread_mutex_init(&loading_man_path_lock,NULL);
	init_context();
	debuging = (int)context->get_value("debuging");
	pthread_mutex_lock(&gtk_lock);
    gtk_init (&argc, &argv);
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

	task_group = task_group_new();
	init_thread(task_group);
	
	init_main_window(window);
	gtk_widget_show(window);
	pthread_mutex_unlock(&gtk_lock);
	//pthread_create(&th_init_data, NULL, (void * (*)(void*))init_man_data, 0);

	//gtk_main();
	
	while(1){
		pthread_mutex_lock(&gtk_lock);
		while(gtk_events_pending()) {
			gtk_main_iteration();
		}
		pthread_mutex_unlock(&gtk_lock);
		usleep(10000);
	}

	return(0);
}

/******************* init_context() *****************/
void init_context()
{
	FILE * fd;
	char buffer[256];
	context = new AppContext();
	//	context->set_default_value("v_size",(void*)400);
	context->set_default_value("debuging","int",(void*)0);
	context->set_default_value("man_paths","char*",(void*)"/usr/man:/usr/local/man:/usr/X11R6/man");
	context->set_default_value("display_section_policy","int",(void*)0);
	context->set_default_value("display_section","int",(void*)3);
	context->set_default_value("searching_mode","int",(void*)0);
	context->set_default_value("show_status_bar","int",(void*)0);
	context->set_default_value("show_warning","int",(void*)0);
	context->set_default_value("show_mode","int",(void*)0); // 0 = xterm, 1 = ghostview
	context->set_default_value("xterm_command","char*",(void*)"xterm"); // rxvt, Eterm also works
	context->set_default_value("gv_command","char*",(void*)"gv");
	context->set_default_value("browser_command","char*",(void*)"netscape"); // kfm also works
	context->set_default_value("cgi_host","char*",(void*)"localhost");
	context->set_default_value("cgi_location","char*",(void*)"/cgi-bin/gman/gman.pl");
	context->set_default_value("print_command","char*",(void*)"lpr"); // not in use

	attach(buffer,getenv("HOME"),".gman");
	if((fd = fopen(buffer,"r"))) {
		context->load(fd);
		fclose(fd);
	}
	if(context->get_value("debuging")) context->display_values();
	//context->save(stdout,"this is just a test");
}



