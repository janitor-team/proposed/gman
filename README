
General Information
===================

This is Gman, a GTK+ front end for man.

Gman's home page will be in http://homex.s-one.net.sg/user/xkwang/gman
(still under construction)

This a little piece of software is mostly developed for the new users of
Unix/Linux.  When I was a newbie of Linux, I felt I always could not find
the man pages that I need.  I believed the man pages must be somewhere in the
system, but I just could not find the name of it.
 
So I thought, maybe we need new graphical man page system.  I don't like
xman because it looks ugly and it is difficult to use (I felt).  I think
gnome-help-browser is too slow and it eats too much memory (about 12M on my
system).  It does not have index search function, and it can not be operated
with keyboard shortcuts.

I think an ideal help system must have the index search function (just like
the help system on windoze).  This will help a newbie to find the
information that s/he needs, and sometimes help him/her go around to see
what other man pages are there in the system, in his leisure time.
 
Gman is nothing else but a simple front-end for the usual man page system.
The most basic job of gman is to build a database for all the man pages
and display them (or part of them) on the screen.  When user decides to read
a man page, gman will launch a xterm window and call the normal man system
to display the man page in the window.  Gman can launch more than one xterm
window at same time.  And user can use the index search function to look for
the man pages that s/he needs.

Installation
============

(possibly editing of Makefile and gman.pl)
$ make
(change to super user if needed)
# make install
 
As default, This will put gman binary in /usr/local/bin, and its manual page
in /usr/local/man/man1.  cgi script in /home/httpd/cgi-bin/gman.

If you don't like the default locations, try to change "prefix" and 
"cgi_bin_prefix" to other values (in Makefile).  And, if your perl
is not in /usr/bin/perl, you may need to modify the first line in gman.pl
accordingly. 

Features of this version
========================

It's easy to write a simple software that can work, but the problem is
in making it work better.  It took me a lot of time to improve it's UI
and some other little things, such as:
 
1. Multi-threading.
   When you have many manual pages on your system, some of the work (such
   as update window) will take a lot of time.  So I used multi-thread
   programming, which means that the user can input new commands at any
   time, without waiting for the previous operation to end.
 
2. ~/.gman file.
   Every time gman starts up, it will read some configuration information
   from $HOME/.gman file.  And every time gman exits, it will also write the
   current configuration information to that same file. ~/.gman file is a
   simple text file and can be edited using any text editor.

To do
=====

 * autoconf support, especially if GNOME support is included.
 * use of gettext for easier translation to other languages.
 * improvment for the parser when reading the ~/.gman file (context.c). 
 * internal method of displaying manual pages, not with xterm.
 
Your comments and suggestions will be the most important for gman
development.  Patches (bug fixes or code additions) are welcome.

Final note
==========

I wonder if it is worthy for me to spend more time on it.  I believe a
user-friendly graphical interface is extremely important for a
software system. Sometimes (often) people find it even more important
than the contents of the software! (windoze is a good example)

I hope more people can benefit because of my work.

/*  gman
 *  Copyright (C) 1999 Xinkai Wang
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/
