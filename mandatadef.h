struct data_pair
{
	char * name;
	int n;
};

#define MAX_MAN_SECTION 15

data_pair sections[]=
{
	{"1",1},
	{"2",1},
	{"3",1},
	{"4",1},
	{"5",1},
	{"6",1},
	{"7",1},
	{"8",1},
	{"l",1},
	{"n",1},
	{"9",1},
	{"tcl",3},
	{"p",1},
	{"o",1},
	{NULL,0}
};

data_pair suffix[]=
{
	{".gz",3},
	{".bz2",4},
	{".z",2},
	{".Z",2},
	{".F",2},
	{".Y",2},
	{NULL,0}
};

