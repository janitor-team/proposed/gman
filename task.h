/************************ task.h ***************************/
/************************ 1999.6.21 ************************/

#ifndef _TASK_H
#define _TASK_H

#include <glib.h>
#include "list.h"

typedef enum
{
  TASK_RUNNING		= 1 << 0,
  TASK_START		= 1 << 1,
  TASK_STOP			= 1 << 2,
} MyTaskRunningFlags;

typedef struct _task_group TaskGroup;
typedef struct _task Task;
typedef int (* TaskRunFunc) (int flag, gpointer data);

TaskGroup * task_group_new();
Task 	* 	task_new(TaskGroup * thread, float priority, TaskRunFunc task_func, gpointer data);
void 		task_signal_send(Task * task, int signal);
void 		task_set_active(Task * task);
void 		task_set_stop(Task * task);

#endif

