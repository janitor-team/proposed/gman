/******************** context.h ********************/

#ifndef _CONTEXT_H
#define _CONTEXT_H

#include <stdio.h>
#include "list.h"

class AppContext
{
 public:
	AppContext();
	~AppContext();
	void * get_value(char * name);
	char * get_value_type(char * name);
	int set_value(char * name, char * type, void * buffer);
	void set_default_value(char *name, char * type, void * buffer);
	/* AppContext::set_value() and set_default_value() only implement simpliest 
	   data management function. If the name or type is in a temp buffer, 
	   it is caller's duty to do strdup() before call the set_value() or 
	   set_default_value() */
	void restore_default(char * name);
	void restore_all();
	void load(FILE *);
	void save(FILE *, char *);
	void display_values();
 private:
	int search_name(List*, char *);
	List * names;
	List * types;
	List * values;
	List * name_default;
	List * type_default;
	List * value_default;
};

#endif
