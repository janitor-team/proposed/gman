#ifndef _TASKFUNC_H_
#define _TASKFUNC_H_
#include "task.h"

extern Task *task_init_man_data;
extern Task *task_extract_man_data;
extern Task *task_add_data_to_clist;
extern Task *task_loading_man_data;
extern Task *task_key_word_search;

void init_thread(TaskGroup * thread);
int init_man_data();

#endif
