# Gman Makefile
# You can adjust the following variables.
CXX = g++
CXXFLAGS = -DVERSION=\"0.9.3\" `gtk-config --cflags` -O2 -Wall
CC = $(CXX) $(CXXFLAGS)

GNOMELIB = #`gnome-config --libs gnomeui`
GTKLIB = -L/usr/lib/ `gtk-config --libs` 
prefix = /usr
cgi_bin_prefix = /var/www/cgi-bin

# There shouldn't be any need to edit anything below this point.
all: gman

gman: menu.o mandata.o util.o gman.o list.o context.o task.o taskfunc.o window2.o
	$(CXX) -lgtk -lgdk -lpthread $(GTKLIB) menu.o mandata.o util.o gman.o list.o context.o task.o taskfunc.o window2.o -o gman

gman.o: gman.c menu.h
	$(CC) -c gman.c

menu.o: menu.c mandata.h util.h
	$(CC) -c menu.c -o menu.o

mandata.o: mandata.c mandata.h util.h mandatadef.h
	$(CC) -c mandata.c -o mandata.o

util.o: util.c util.h
	$(CC) -c util.c -o util.o

list.o: list.c list.h
	$(CC) -c list.c

context.o: context.c context.h
	$(CC) -c context.c

task.o: task.c task.h
	$(CC) -c task.c

taskfunc.o: taskfunc.c taskfunc.h
	$(CC) -c taskfunc.c

window2.o: window2.c window2.h
	$(CC) -c window2.c

clean: 
	rm -f *.o *~ gman

install:
	test -d $(prefix)/bin || mkdir -p $(prefix)/bin
	test -d $(prefix)/man/man1 || mkdir -p $(prefix)/man/man1
	test -d $(cgi_bin_prefix)/gman || mkdir -p $(cgi_bin_prefix)/gman
	install -s -m 755 gman $(prefix)/bin
	install -s -m 755 gman.pl $(prefix)/bin/gman.cgi
	install -m 644 gman.1x $(prefix)/man/man1
	install -s -m 755 gman.pl $(ci_bin_prefix)/gman
